import React from 'react';
import Header from '../header';
import styles from './styles.module.css';
import Logo from '../logo';
import MessageList from '../message_list';
import MessageInput from '../message_input';

class Chat extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      messages: []
    };
  }

  componentDidMount() {
    this.getAvailableMessages()
      .then(messagesData => {
        console.log(`messages received size=${messagesData.length}`);
        this.setState({
          messages: messagesData
        });
      })
  }

  componentWillUnmount() {
  }

  // returns Promise of [Message]
  getAvailableMessages() {
    const request = new Request('https://api.npoint.io/b919cb46edac4c74d0a8', {
      method: 'GET'
    });
    return fetch(request)
      .then(response => response.json())
  }

  render() {
    const { messages } = this.state;

    return (
      <>
        <Logo />
        <div className={styles.content_container}>
          <Header className={styles.header_content} chatName="My Chat" participants={23} messages={53} messageTime={23.15} />

          <MessageList messages={messages} />

          <div className={styles.message_bar}>
            <MessageInput />
            <button className={styles.send_btn}>Send</button>
          </div>
        </div>

      </>
    );
  }

}

export default Chat;

