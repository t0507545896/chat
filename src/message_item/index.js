import React from 'react';
import style from './style.module.css';
import classnames from 'classnames';



const MessageItem = (props) => {
    const { avatar, text, createdAt } = props.message;
    const combinedMessList = classnames([props.className , style.message_item])
    return (
        <div className={combinedMessList}>
            <img className={style.avatar} src={avatar} alt="avatar" />
            <div className={style.message_details}>
                <div className={style.text}>{text}</div>
                <div className={style.date_like_items}>
                    <div className={style.createdAt}>{createdAt}</div>
                    <button className={style.like_btn}>Like</button>
                </div>
            </div>
            
        </div>
    )
}

export default MessageItem;

// id
//  text
//  user
//  avatar
// userId
// editedAt
// createdAt