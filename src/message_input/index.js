import React from 'react';
import style from './style.module.css';

const MessageInput = () => {
    return (
        <>
            <input type="text" placeholder="Message"  className={style.message_input}></input>
            
        </>
    )
}
export default MessageInput;
