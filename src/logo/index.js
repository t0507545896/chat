import React from 'react';
import styles from './styles.module.css';
import LogoImg from './envelop.png';

const Logo = () => {
    return (
        <div className={styles.logo}>
            <img className={styles.logo_image} src={LogoImg} alt="logo" />
        </div>
    );
}

export default Logo;
